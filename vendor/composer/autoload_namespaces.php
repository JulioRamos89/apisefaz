<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'HTMLPurifier' => array($vendorDir . '/ezyang/htmlpurifier/library'),
    'ForceUTF8\\' => array($vendorDir . '/neitanod/forceutf8/src'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
    'Codeception\\' => array($vendorDir . '/codeception/specify/src'),
    'Behat\\Gherkin' => array($vendorDir . '/behat/gherkin/src'),
);
